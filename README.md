# Тестовое приложение для Wheely #

Подключение по протоколу WebSocket. Авторизация, отправка текущих координат, получение списка координат и отображение их на карте Google Maps.

## Сборка ##

### Тестовая сборка (вывод подробных логов) ###

$ ./gradlew clean installDebug

### Релизная сборка ###
$ ./gradlew clean installRelease

## Интерфейс ##
Чекбокс в правой части тулбара - включение/отключение сервиса.
При разрыве соединения/отключении сервиса карта очищается от меток. Запускается монитор отслеживания подключения сокета.
Логин и пароль сохраняются для текущего работающего экземпляра приложения.
Если карта Google Maps не отображается, то скорее всего проблема с ключами при сборке.
После регистрации нового ключа в консоли Google обязательно необходимо перезалить приложение на устройстве.
Отдельно выложил релизную [сборку](https://bitbucket.org/Tepex/wheely/downloads/wheely-release-1.1.25.apk)

## Используемые библиотеки ##
* [Gson](https://github.com/google/gson) - сериализация/десериализация координат
* [nv-websocket-client](https://github.com/TakahikoKawasaki/nv-websocket-client) - работа с WebSocket
* [EventBus](https://github.com/greenrobot/EventBus) - взаимодействие между сервисом и активити
* [GoogleMap](https://developers.google.com/android/reference/com/google/android/gms/maps/GoogleMap) - карта Google Maps
* [LocationServices](https://developers.google.com/android/reference/com/google/android/gms/location/LocationServices) - геолокация
* [RetroLambda](https://github.com/orfjackal/retrolambda) - лямбда-выражения (Java 8)  для Java 7.