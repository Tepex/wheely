package com.wheely.test;

import android.app.Application;

public class TestApplication extends Application
{
	public String getUsername()
	{
		return username;
	}
	
	public void setUsername(String username)
	{
		this.username = username;
	}
	
	public String getPassword()
	{
		return pwd;
	}
	
	public void setPassword(String pwd)
	{
		this.pwd = pwd;
	}
	
	private String username;
	private String pwd;
}