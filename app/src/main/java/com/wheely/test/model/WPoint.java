package com.wheely.test.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.location.Location;

public class WPoint implements Parcelable
{
	public WPoint(int id, Location location)
	{
		this.id = id;
		lat = location.getLatitude();
		lon = location.getLongitude();
	}
	
	public WPoint(Parcel parcel)
	{
		id = parcel.readInt();
		lat = parcel.readDouble();
		lon = parcel.readDouble();
		str = "id="+id+" lat,lon="+lat+","+lon;
	}
	
	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(id);
		dest.writeDouble(lat);
		dest.writeDouble(lon);
	}

	@Override
	public int describeContents()
	{
		return 0;
	}
	
	@Override
	public int hashCode()
	{
		return id;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj == null) return false;
		if(!(obj instanceof WPoint)) return false;
		WPoint other = (WPoint)obj;
		return id == other.id;
	}
	
	@Override
	public String toString()
	{
		return str;
	}
	
	public int getId()
	{
		return id;
	}
	
	public double getLatitude()
	{
		return lat;
	}
	
	public double getLongitude()
	{
		return lon;
	}
	
	private int id;
	private double lat;
	private double lon;
	private transient String str;
	
	public static final Parcelable.Creator<WPoint> CREATOR = new Parcelable.Creator<WPoint>()
	{
		@Override
		public WPoint createFromParcel(Parcel in)
		{
			return new WPoint(in);
		}
			
		@Override
		public WPoint[] newArray(int size)
		{
			return new WPoint[size];
		}
	};
}
