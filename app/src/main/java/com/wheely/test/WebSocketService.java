package com.wheely.test;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;

import android.content.Intent;

import android.location.Location;

import android.os.Bundle;
import android.os.IBinder;

import android.support.annotation.CallSuper;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.neovisionaries.ws.client.OpeningHandshakeException;
import com.neovisionaries.ws.client.StatusLine;
import com.neovisionaries.ws.client.WebSocket;
import com.neovisionaries.ws.client.WebSocketAdapter;
import com.neovisionaries.ws.client.WebSocketException;
import com.neovisionaries.ws.client.WebSocketFactory;
import com.neovisionaries.ws.client.WebSocketFrame;

import com.wheely.test.model.WPoint;
import com.wheely.test.ui.MainActivity;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;

import java.lang.reflect.Modifier;

import java.util.List;
import java.util.Map;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.wheely.test.Utils.TAG;

public class WebSocketService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener
{
	@Override
	public void onCreate()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "service on create");
		client = new GoogleApiClient.Builder(this)
			.addApi(LocationServices.API)
			.addConnectionCallbacks(this)
			.addOnConnectionFailedListener(this)
			.build();
		webSocketFactory = new WebSocketFactory();
		webSocketFactory.setConnectionTimeout(CONNECTION_TIMEOUT);
		gson = new GsonBuilder()
			.excludeFieldsWithModifiers(Modifier.STATIC, Modifier.TRANSIENT)
			.create();
	}
	
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		this.startId = startId;
		client.connect();
		if(BuildConfig.DEBUG) Log.d(TAG, "onStartCommand "+intent+" startId="+startId);
		if(intent == null) return START_STICKY;
		TestApplication app = (TestApplication)getApplication();
		String username = intent.getStringExtra(EXTRA_USERNAME);
		if(username != null)
		{
			app.setUsername(username);
			app.setPassword(intent.getStringExtra(EXTRA_PWD));
		}
		
		if(BuildConfig.DEBUG) Log.d(TAG, "onStartCommand. username: "+username+" isConnected "+intent.getBooleanExtra(EXTRA_CONNECTED, false)+" location: "+location);
		
		try
		{
			webSocketConnect();
		}
		catch(Exception e)
		{
			Log.e(TAG, "Web socket connection error", e);
			EventBus.getDefault().post(new ConnectionEvent(false, e.getMessage(), null));
			ws.clearListeners();
			ws = null;
			return START_NOT_STICKY;
		}
		
		if(intent.getBooleanExtra(EXTRA_CONNECTED, false))
		{
			connected = true;
			sendLocationToServer(location);
			EventBus.getDefault().post(new LocationEvent(null, location));
		}
		return START_STICKY;
	}
	
	@CallSuper
	@Override
	public void onDestroy()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "destroy service");
		if(ws != null)
		{
			disconnect = true;
			ws.clearListeners();
			ws.disconnect();
			ws = null;
		}
		if(client.isConnected())
		{
			LocationServices.FusedLocationApi.removeLocationUpdates(client, this);
			client.disconnect();
		}
		EventBus.getDefault().post(new ConnectionEvent(false, "destroy service", null));
		ws = null;
		super.onDestroy();
	}
	
	@Override
	public IBinder onBind(Intent intent)
	{
		return(null);
	}
	
	@Override
	public void onConnected(Bundle bundle)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Google API connected");
		locationRequest = LocationRequest.create();
		locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locationRequest.setSmallestDisplacement(SMALLEST_DISPLACEMENT);
		LocationServices.FusedLocationApi.requestLocationUpdates(client, locationRequest, this);
		location = LocationServices.FusedLocationApi.getLastLocation(client);
		if(location != null)
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "getLastLocation "+location);
			postLocation(location);
		}
	}
	
	@Override
	public void onConnectionSuspended(int cause)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Google API connection suspended: "+cause);
	}
	
	@Override
	public void onConnectionFailed(ConnectionResult result)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Google API connection failed");
	}
	
	@Override
	public void onLocationChanged(Location location)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Location changed: "+location);
		postLocation(location);
	}
	
	private void postLocation(Location location)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "postLocation: "+location+" connected: "+connected);
		EventBus.getDefault().post(new LocationEvent(null, location));
		if(connected) sendLocationToServer(location);
	}
	
	private void webSocketConnect() throws Exception
	{
		if(ws != null && ws.isOpen())
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "WS connection already opened");
			return;
		}
		TestApplication app = (TestApplication)getApplication();
		String uri = getString(R.string.URI)+"?"+QUERY_USERNAME+"="+app.getUsername()+"&"+QUERY_PASSWORD+"="+app.getPassword();
		if(BuildConfig.DEBUG) Log.d(TAG, "Connecting to "+uri);
		ws = webSocketFactory.createSocket(uri);
		ws.addListener(webSocketAdapter);
		ws.connectAsynchronously();
	}
	
	private void connected()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "WebSocket connected");
		EventBus.getDefault().post(new ConnectionEvent(true, "Connection OK", location));
		sendLocationToServer(location);
		Intent intent = new Intent(this, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

		Notification noti = new Notification.Builder(this)
			.setAutoCancel(false)
			.setContentTitle(getString(R.string.notification_title))
			.setSmallIcon(R.drawable.ic_notification)
			.setOngoing(true)
			.setContentIntent(PendingIntent.getActivity(this, 0, intent, 0))
			.build();
		noti.flags |= Notification.FLAG_NO_CLEAR;
		startForeground(NOTIFICATION_ID, noti);
	}
	
	private void sendLocationToServer(Location location)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "sendLocationToServer: "+location+" connected "+connected);
		if(ws == null || !ws.isOpen() || !connected || location == null) return;
		WPoint point = new WPoint(0, location);
		String json = gson.toJson(point);
		if(BuildConfig.DEBUG) Log.d(TAG, "Sending: "+json);
		ws.sendText(json);
	}
	
	private int startId;
	private LocationRequest locationRequest;
	private GoogleApiClient client;
	private Location location;
	private WebSocketFactory webSocketFactory;
	private WebSocket ws;
	private WebSocketAdapter webSocketAdapter = new MyWebSocketAdapter();
	private volatile boolean disconnect;
	private volatile boolean connected;
	private Gson gson;
	private ScheduledExecutorService service;
	private volatile boolean tryConnect;
	
	public static final int NOTIFICATION_ID = 12312;
	public static final int CONNECTION_TIMEOUT = 5000;
	public static final long CONNECTION_MONITOR_DELAY = 3L;
	
	public static final String EXTRA_USERNAME = "username";
	public static final String EXTRA_PWD = "pwd";
	public static final String EXTRA_LAST_LOCATION = "location";
	public static final String EXTRA_CONNECTED = "connected";
	
	public static final String QUERY_USERNAME = "username";
	public static final String QUERY_PASSWORD = "password";
	/** Минимальное расстояние в метрах, через которое отправлять уведомление об изменении координат */
	public static final int SMALLEST_DISPLACEMENT = 100;
	
	class MyWebSocketAdapter extends WebSocketAdapter
	{
		@Override
		public void onError(WebSocket ws, WebSocketException cause) throws Exception
		{
			Log.e(TAG, "WebSocket error.", cause);
			if(BuildConfig.DEBUG) Log.d(TAG, "WebSocket error. tryConnect="+tryConnect);
			if(tryConnect) return;
			String msg = null;
			boolean is403 = false;
			if(cause instanceof OpeningHandshakeException)
			{
				StatusLine sl = ((OpeningHandshakeException)cause).getStatusLine();
				if(sl.getStatusCode() == 403)
				{
					msg = getString(R.string.signin_error);
					is403 = true;
				}
			}
			if(msg == null) msg = cause.getMessage();
			EventBus.getDefault().post(new ConnectionEvent(false, msg, null));
			if(BuildConfig.DEBUG) Log.d(TAG, "WebSocket error. is403="+is403);
			if(!is403)
			{
				tryConnect = true;
				if(WebSocketService.this.ws != null)
				{
					WebSocketService.this.ws.clearListeners();
					disconnect = true;
					if(ws.isOpen()) ws.disconnect();
					WebSocketService.this.ws = null;
				}
				
				//service.awaitTermination(CONNECTION_MONITOR_DELAY, TimeUnit.SECONDS);
				if(BuildConfig.DEBUG) Log.d(TAG, "WebSocket error. Start scheduled executor service");
				if(service != null) service.shutdownNow();
				service = Executors.newSingleThreadScheduledExecutor();
				service.scheduleAtFixedRate(new ConnectionMonitor(), 0L, CONNECTION_MONITOR_DELAY, TimeUnit.SECONDS);
			}
		}
				
		@Override
		public void onConnected(WebSocket ws, Map<String,List<String>> headers) throws Exception
		{
			connected();
			if(tryConnect)
			{
				tryConnect = false;
				service.shutdownNow();
				service.awaitTermination(CONNECTION_MONITOR_DELAY, TimeUnit.SECONDS);
			}
		}
				
		@Override
		public void onDisconnected(WebSocket ws, WebSocketFrame serverCloseFrame, WebSocketFrame clientCloseFrame, boolean closedByServer) throws Exception
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "WebSocket disconnected. tryConnect="+tryConnect);
			if(tryConnect) return;
			connected = false;
			if(!disconnect)
			{
				WebSocketService.this.ws.clearListeners();
				WebSocketService.this.ws = null;
				webSocketConnect();
			}
		}
		
		@Override
		public void onTextMessage(WebSocket ws, String text) throws Exception
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "Received "+text);
			WPoint[] points = gson.fromJson(text, WPoint[].class);
			EventBus.getDefault().post(new LocationEvent(points, null));
		}
	}
	
	class ConnectionMonitor implements Runnable
	{
		@Override
		public void run()
		{
			if(ws != null) ws.clearListeners();
			ws = null;
			tryConnect = true;
			try
			{
				webSocketConnect();
			}
			catch(Exception e)
			{
				if(BuildConfig.DEBUG) Log.e(TAG, "TimerTask: Web socket connection error", e);
				ws.clearListeners();
				ws = null;
			}
		}
	}
	
	public static class ConnectionEvent
	{
		public ConnectionEvent(boolean isOk, String message, Location location)
		{
			this.isOk = isOk;
			this.message = message;
			this.location = location;
		}
		
		public boolean isConnected()
		{
			return isOk;
		}
		
		public String getMessage()
		{
			return message;
		}
		
		public Location getLocation()
		{
			return location;
		}
		
		private boolean isOk;
		private String message;
		private Location location;
	}
	
	public static class LocationEvent
	{
		/**
		 * Сообщение с координатами.
		 *
		 * @param points список координат
		 * @param isMyLocation true - отправляются текущие координаты устройства первым элементом в списке. false - отправляется список координат с сервера.
		 */
		public LocationEvent(WPoint[] points, Location myLocation)
		{
			this.points = points;
			this.myLocation = myLocation;
		}
		
		public WPoint[] getPoints()
		{
			return points;
		}
		
		public Location getMyLocation()
		{
			return myLocation;
		}
		
		private WPoint[] points;
		private Location myLocation;
	}
}
