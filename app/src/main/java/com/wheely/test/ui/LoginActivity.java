package com.wheely.test.ui;

import android.annotation.SuppressLint;

import android.app.ProgressDialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;

import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wheely.test.BuildConfig;
import com.wheely.test.R;
import com.wheely.test.Utils;
import com.wheely.test.WebSocketService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.wheely.test.Utils.TAG;

public class LoginActivity extends AppCompatActivity
{
	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(BuildConfig.DEBUG) Log.d(TAG, "Login activity onCreate. task id: "+getTaskId());
		setContentView(R.layout.login_layout);
		loginButton = (Button)findViewById(R.id.bt_signin);
		if(savedInstanceState != null && savedInstanceState.getBoolean(SAVE_SHOW_PROGRESS_DIALOG)) showProgressDialog();
	}
	
	@CallSuper
	@Override
	protected void onStart()
	{
		super.onStart();
		if(BuildConfig.DEBUG) Log.d(TAG, "Login activity onStart. task id: "+getTaskId());
		EventBus.getDefault().register(this);
	}
	
	@CallSuper
	@Override
	protected void onStop()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "Login activity onStop. task id: "+getTaskId());
		EventBus.getDefault().unregister(this);
		super.onStop();
	}
	
	@CallSuper
	@Override
	public void onSaveInstanceState(Bundle bundle)
	{
		boolean isShowing = false;
		if(progressDialog != null) isShowing = progressDialog.isShowing();
		bundle.putBoolean(SAVE_SHOW_PROGRESS_DIALOG, isShowing);
		super.onSaveInstanceState(bundle);
	}
	
	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onConnectionEvent(WebSocketService.ConnectionEvent event)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "new connection event. "+event.getMessage()+". Location: "+event.getLocation());
		cancelProgressDialog();
		if(event.isConnected())
		{
			Intent intent = new Intent();
			if(event.getLocation() != null) intent.putExtra(WebSocketService.EXTRA_LAST_LOCATION, event.getLocation());
			setResult(RESULT_OK, intent);
			finish();
		}
		else Toast.makeText(this, event.getMessage(), Toast.LENGTH_LONG).show();
	}
	
	public void connect(View view)
	{
		showProgressDialog();
		EditText loginEdit = (EditText)findViewById(R.id.et_login);
		EditText pwdEdit = (EditText)findViewById(R.id.et_pwd);
		
		String username = loginEdit.getText().toString();
		String pwd = pwdEdit.getText().toString();
		
		Intent intent = new Intent(this, WebSocketService.class);
		intent.putExtra(WebSocketService.EXTRA_USERNAME, username);
		intent.putExtra(WebSocketService.EXTRA_PWD, pwd);
		if(BuildConfig.DEBUG) Log.d(TAG, "start service");
		startService(intent);
	}
	
	public void showProgressDialog()
	{
		canceledByService = false;
		progressDialog = ProgressDialog.show(this, getString(R.string.signin), getString(R.string.wait), true, true, (DialogInterface dialog)->
			{
				if(BuildConfig.DEBUG) Log.d(TAG, "cancel progress dialog. canceledByService "+canceledByService);
				if(!canceledByService)
				{
					stopService(new Intent(this, WebSocketService.class));
					setResult(RESULT_CANCELED);
					finish();
				}
				loginButton.setEnabled(true);
			});
		loginButton.setEnabled(false);
	}
	
	public void cancelProgressDialog()
	{
		canceledByService = true;
		if(progressDialog != null) progressDialog.cancel();
		loginButton.setEnabled(true);
	}
	
	private ProgressDialog progressDialog;
	private Button loginButton;
	private boolean canceledByService;
	
	public static final String SAVE_SHOW_PROGRESS_DIALOG = "progress";
}
