package com.wheely.test.ui;

import android.annotation.SuppressLint;

import android.app.ActivityManager;

import android.content.Context;
import android.content.Intent;

import android.location.Location;

import android.media.Ringtone;
import android.media.RingtoneManager;

import android.net.Uri;

import android.os.Bundle;

import android.support.annotation.CallSuper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.util.Log;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.wheely.test.BuildConfig;
import com.wheely.test.R;
import com.wheely.test.TestApplication;
import com.wheely.test.Utils;
import com.wheely.test.WebSocketService;
import com.wheely.test.model.WPoint;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.wheely.test.Utils.TAG;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback
{
	@CallSuper
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		if(((TestApplication)getApplication()).getUsername() == null)
		{
			if(BuildConfig.DEBUG) Log.d(TAG, "Start login activity");
			startActivityForResult(new Intent(this, LoginActivity.class), 1);
		}
		setContentView(R.layout.main_layout);
		toolbar = (Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		getSupportActionBar().setHomeButtonEnabled(false);
		getSupportActionBar().show();
		((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
		
		if(savedInstanceState != null)
		{
			connected = savedInstanceState.getBoolean(WebSocketService.EXTRA_CONNECTED);
			savedPoints = (WPoint[])savedInstanceState.getParcelableArray(SAVED_POINTS);
		}
	}
	
	@CallSuper
	@Override
	protected void onStart()
	{
		super.onStart();
		if(BuildConfig.DEBUG) Log.d(TAG, "MainActivity.onStart(). connected "+connected+". location "+location);
		setServiceIcon();
		EventBus.getDefault().register(this);
		if(((TestApplication)getApplication()).getUsername() != null) connected = true;
		if(connected)
		{
			Intent intent = new Intent(this, WebSocketService.class);
			intent.putExtra(WebSocketService.EXTRA_CONNECTED, true);
			if(BuildConfig.DEBUG) Log.d(TAG, "start service for connected");
			startService(intent);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "onActivityResult. code: "+resultCode);
		if(resultCode == RESULT_OK)
		{
			location = (Location)data.getParcelableExtra(WebSocketService.EXTRA_LAST_LOCATION);
			if(BuildConfig.DEBUG) Log.d(TAG, "onActivityResult location="+location);
			setCurrentLocation();
			connected = true;
		}
		else finish();
	}
	
	@CallSuper
	@Override
	protected void onStop()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "MainActivity.onStop()");
		EventBus.getDefault().unregister(this);
		super.onStop();
	}
	
	@CallSuper
	@Override
	public void onSaveInstanceState(Bundle bundle)
	{
		bundle.putBoolean(WebSocketService.EXTRA_CONNECTED, connected);
		bundle.putParcelableArray(SAVED_POINTS, markers.keySet().toArray(new WPoint[0]));
		super.onSaveInstanceState(bundle);
	}
	
	@Override
	public void onMapReady(GoogleMap map)
	{
		this.map = map;
		map.setMyLocationEnabled(true);
		if(location != null) setCurrentLocation();
		if(savedPoints != null) setMarkers(savedPoints);
	}
	
	@CallSuper
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		if(BuildConfig.DEBUG) Log.d(TAG, "onCreateOptionsMenu");
		getMenuInflater().inflate(R.menu.main_options, menu);
		this.menu = menu;
		setServiceIcon();
		return true;
	}
	
	@CallSuper
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if(item.getItemId() == R.id.action_toggle)
		{
			Intent intent = new Intent(this, WebSocketService.class);
			intent.putExtra(WebSocketService.EXTRA_CONNECTED, true);
			if(isServiceRunning())
			{
				stopService(intent);
				item.setIcon(R.drawable.ic_stopped);
				connected = false;
				markers.clear();
				map.clear();
			}
			else
			{
				startService(intent);
				item.setIcon(R.drawable.ic_started);
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onLocationEvent(WebSocketService.LocationEvent event)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "new location event");
		if(event.getMyLocation() != null)
		{
			location = event.getMyLocation();
			setCurrentLocation();
		}
		else setMarkers(event.getPoints());
	}
	
	@Subscribe(threadMode = ThreadMode.MAIN)
	public void onConnectionEvent(WebSocketService.ConnectionEvent event)
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "connection event "+event.isConnected());
		connected = event.isConnected();
		if(!connected)
		{
			markers.clear();
			map.clear();
		}
	}
	
	private void setCurrentLocation()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "setCurrentLocation map: "+map);
		if(map == null) return;
		LatLng my = new LatLng(location.getLatitude(), location.getLongitude());
		map.moveCamera(CameraUpdateFactory.newLatLngZoom(my, 10));
	}

	private boolean isServiceRunning()
	{
		ActivityManager manager = (ActivityManager)getSystemService(Context.ACTIVITY_SERVICE);
		for(ActivityManager.RunningServiceInfo service: manager.getRunningServices(Integer.MAX_VALUE))
		{
			if(service.service.getClassName().equals(WebSocketService.class.getName())) return true;
		}
		return false;
	}
	
	private void setServiceIcon()
	{
		if(BuildConfig.DEBUG) Log.d(TAG, "setServiceIcon "+menu);
		if(menu == null) return;
		MenuItem item = menu.findItem(R.id.action_toggle);
		if(BuildConfig.DEBUG) Log.d(TAG, "setServiceIcon. item "+item);
		if(item == null) return;
		if(isServiceRunning()) item.setIcon(R.drawable.ic_started);
		else item.setIcon(R.drawable.ic_stopped);
	}
	
	private void setMarkers(WPoint[] points)
	{
		if(points == null) return;
		for(WPoint point: points)
		{
			LatLng position = new LatLng(point.getLatitude(), point.getLongitude());
			Marker marker = markers.get(point);
			if(marker == null)
			{
				marker = map.addMarker(new MarkerOptions().position(position).title(""+point.getId()));
				markers.put(point, marker);
			}
			else marker.setPosition(position);
		}
	}
	
	private Toolbar toolbar;
	private Menu menu;
	private GoogleMap map;
	private Map<WPoint, Marker> markers = new HashMap<WPoint, Marker>();
	private WPoint[] savedPoints;
	private Location location;
	private boolean connected;
	
	public static final String SAVED_POINTS = "points";
}
